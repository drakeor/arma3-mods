This mod brings the M16 pack into the game.

Changelog:
v1.1
-Fixed Hand Animation so that it doesn't go through the weapon on regular M16A4
-Added M16A4_M203
-Improved sounds thanks to oliie
-Fixed Reload Animation
-Unloaded M16 now has no Magazine

Credits:
Bohemia Interactive
Toadball
oliie
Zach Gibson

Classlist

M16A4: Shm_m16a4
M16A4 M203: Shm_m16a4_m203
30 Round Stanag Mag: 30rnd_556x45_stanag
30 Round stanag Tracer Mag: 30rnd_556x45_t_stanag

By Shamwill