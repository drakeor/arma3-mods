

Landtex A3A v0.7
-------------------




Description:
This Addon makes the Ground Textures more flat. So that they no longer look like a Satellite Image.


Installation:
Extract into the ArmA 3 Directory, by default this is located in:
32-Bit - C:/Program Files/Steam/Steamapps/Common/ArmA 3/
64-Bit - C:/Program Files (x86)/Steam/Steamapps/Common/ArmA 3/
After extraction it should look like this:

Steam/Steamapps/Common/ArmA 3/@your_mod_folder_name


You'll also need add a Launch Parameter to Steam, in order to do so right-click on ArmA 3 Alpha and 
click Properties and then Set Launch Options. In the window that opens enter in -mod=@your_mod_folder_name
For using multiple mods you would then do so like this:

-mod=@mod_name;@mod_name2;@mod_name3;@mod_name4;@mod_name5


Note:
You can also use -nosplash to get rid of the splash art and intro videos.





Picolly