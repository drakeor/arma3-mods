ADDON : FA_gps made by Head
Version : 0.1

Release Date :08/03/2013

======================
CONTACT INFORMATION :
======================

headswe92 at gmail.com




======================
REQUIRED ADDONS:
======================

None




======================
DESCRIPTION :
======================

Moves GPS to the top right under the ammo dialog.




==============================
CHANGELOG/BUGS/KNOWN ISSUES: 
==============================

v0.1
first release





======================
INSTALLATION :
======================

The PBO and Sign files in the archive need to be extracted into your ArmA/Addons folder.
However the mod folder method is the better solution. 
Copy the following URL into your browser to view a Mod Folder Tutorial : http://tinyurl.com/armamodfolder




======================
CREDITS AND THANKS :
======================

Thanks to FOLK ARPS.





======================
DISCLAIMER :
======================

I take no responsibility for (im)possible damage to your game/system that may be caused 
by installation of this Addon. This Addon is also prohibited to be used in any commercial product.

This readme has been made by using the ArmedAssault.info Readme Generator.
Check ArmedAssault.info to get the latest ArmA news !