Beta release, recommended for testing

Launch with -mod=@CBA_A3



*** KNOWN ISSUES ***


Arma 3 Alpha STABLE BRANCH - Build 0.56.104778
_______________________________________________________________

* A3 CBA Causing RPT spam - https://dev-heaven.net/issues/71853




Arma 3 Alpha DEVELOPMENT BRANCH - Build 0.55.104711
_______________________________________________________________

* A3 CBA Causing RPT spam - https://dev-heaven.net/issues/71853





*** WE NEED FEEDBACK ON ***
_______________________________________________________________

* https://dev-heaven.net/issues/71375 - CBA for A3 Causes Radio Messages to Play Twice on Infantry and Helicopter Showcases