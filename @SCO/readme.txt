AntoineFlemming's SCO Joint Forces Command [ALPHA]
*************************
Version 0.57
*************************
Required Addons: AntoineFlemming's NATO Special Operations Command [ALPHA], Sudden's Russians, majoris' China - People's Liberation Army Infantry [ALPHA]
Installation:
-Put the .pbo files in your ArmA3 AddOns folder. The use of mod folders is strongly encouraged.
*************************
Here is the Alpha version of my SCO Joint Forces Command Addon, which features uniform, vest, and helmet reskins for Iranian, Takistani, Russian, Chinese, and Hezbollah infantry and special forces.

Credits:
-Bohemia Interactive
-Kiory (balaclava)
-majoris
-sudden
*************************