This is my port of Christian.1987's mp7, originally released for Arma2.

The weapon has fully functional modular attachments (supressor, flashlight, ir laser, optics systems) and working muzzle flash.
It does not feature A3 reload animations (yet).

Classnames:

C1987_MP7
C1987_Mp7_Holo
C1987_Mp7_Holo_point
C1987_Mp7_Holo_point_snds
C1987_Mp7_ACO_point
C1987_Mp7_ACOg_point
C1987_Mp7_ARCO_point
C1987_Mp7_Hamr_point
C1987_Mp7_ACO_flash
C1987_Mp7_Holo_flash
C1987_Mp7_Holo_flash_snds


Magazines:
40Rnd_46x30_mp7
20Rnd_46x30_mp7

Ammo box:
C1987_Mp7_Box


Credits:

Chrisitan1987
His original A2 release thread is here:
http://forums.bistudio.com/showthread.php?90556-Christian-1987-AddOn-Release-Thread&p=2130298&viewfull=1#post2130298

My and Christian's thanks go to:
'Schmung' for the Mp7 ground base Model !
'The_Tub' for the Mp7 Texture base !
'TheLama' for the Acog/EoTech Model & Texture's !
'Norrin' for the PERFECT Hand-Hold Animation !
'da12thMonkey' for the AWESOME LLM-01 Laser/Light Module Model & Texture!
'LordJarhead' for the AWESOME Sound's & Sound Config Edit!

I personally want to thank: 
1. Chrisitan for giving me permission to port this outstanding addon
2. Alwarren, who has beem guiding me every step of the way
3. Robert Hammer for advice.