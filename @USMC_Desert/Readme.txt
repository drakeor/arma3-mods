// Description

Retexture of the MTP uniform, plates and helmet styled as US Marine Desert MARPAT.


// Included Files

@USMC_Desert/Addons/usmc_desert.pbo
@USMC_Desert/Addons/usmc_desert.pbo.MCS_USMC.bisign
@USMC_Desert/Server Key/MCS_USMC.bikey
@USMC_Desert/readme.txt


// Install

Extract the contents of USMC_Desert_1.1.zip to your ArmA 3 DIR.

C:\Program Files\Steam\steamapps\common\Arma 3

Your location may differ to the above.

Go to your steam library. Right click ArmA 3 Alpha and click properties then select Set Launch Options.

Enter the line below to run the USMC Desert addon.

-mod=@USMC_Desert

If you have more than one addon seperate each addon by a ;

-mod=@USMC_Desert;@Addon2;@Addon3


// Usage

To use the USMC unit in the mission editor set the side to BluFor and Faction to US Marines (Desert).
Currently there is only one Unit which is Rifleman.

All the loadouts are separate and can be loaded into ammo crates or placed on any units.

Name
Class

USMC Uniform (Desert)
USMC_DESERT_Uniform

USMC Plate Carrier (Desert)
USMC_DESERT_Plate_Carrier

USMC Helmet (Desert)
USMC_DESERT_Helmet

Changing a players uniform
// Remove the unjform first then add the USMC one.
removeUniform this; this addUniform "USMC_DESERT_Uniform";

Adding 10 uniforms to Ammo box.
// Box AddItems ["USMC uniform", How Many];
this addItemCargo ["USMC_DESERT_Uniform", 10];


// Credits
Scarecrow398 - Help with the config and texture advise.
Bohemia Interactive - Base models and textures.
Goodson [3CB] - 1337 Camera Skillz


// Changelog

Ver 1.1
 - Added reskinned ballistic helmet and icon.

Ver 1.0
 - Added reskinned texture for the MTP Uniform and Plates.