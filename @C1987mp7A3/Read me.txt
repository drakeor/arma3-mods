This is my port of Christian.1987's mp7, originally released for Arma2.

The weapon has fully functional modular attachments (supressor, flashlight, ir laser, optics systems) and working muzzle flash.

Classnames:

C1987_MP7
C1987_MP7_zp

camo variants:

C1987_Mp7_dmarpat
C1987_Mp7_marpat
C1987_Mp7_nwu
C1987_Mp7_sand
C1987_Mp7_ucp
C1987_Mp7_urb
C1987_Mp7_wcam


note:
"C1987_MP7_zp" has non-modular sight. See changelog


Attachments:
C1987_MP7_suppressor

Magazines:
C1987_40Rnd_46x30_mp7
C1987_20Rnd_46x30_mp7

Ammo box:
C1987_Mp7_Box


!!!!!!!!!!!!!!!!!ATTENTION: ADDITIONAL CONFIG!!!!!!!!!!!!!!!!

If you have the R3F Weapon Pack you should replace the vanilla config ("c1987_mp7_c_vanilla.pbo") with c1987_mp7_c_R3F.pbo (provided inside the "config" sub-folder). This will replenish your ammo box with attachments from the R3F Pack. Suppressors from the R3F pack have been renamed to: "C1987_MP7_HK_suppressor"
"C1987_MP7_HK_DES_suppressor"
this has been done to preserve the ballistics and recoil of the default suppressor.

The most excellent R3F Pack can be found here: http://forums.bistudio.com/showthread.php?152761-R3F-French-Weapons-Pack-ALPHA


CHANGELOG:

2.0
-Added new textures for different camo variants: created by Pomigit! (I think this warrants a bump straight to version 2.0).
-removed some weapon variants from the crate (it was getting crowded in there)

1.4
- Rotating muzzle flash
- Adjustments to attachment positions
- Compatibility with Alwarren's updated FHQ Accessories Pack: http://forums.bistudio.com/showthread.php?153794-FHQ-Accessories-pack

1.3
- Top proxy (for sights) slightly adjusted (centered)
- Holo and backup sights should now work in a similar way as with other BIS weapons
- additional config provided to extend compatibility (with attachments from R3F weapon pack)

1.2
- side attachments moved to the right side, as in default BIS weapons
- slightly increased damage
- better reload animation (from the BIS MX)
- signed and server key included

1.1
- Replaced rifle suppressor with pistol suppressor
- Removed unnecessary SD mags
- Added another class:"C1987_Mp7_ACO_point_snds"
- Deleted a left-over proxy (triangle that popped up near the muzzle at a certain distance [LOD])
- added a variant with Zeiss Z-point sight that was featured in Christian's original release. Unfortunatelly Z-point is not modular (you cannot disasemble it from the weapon).
- Added images for weapons (they are not very accurate, though).
- Config rewritten by Robalo!
- Some changes to class names (see updates class names above).


1.0
First release

Credits:

Chrisitan1987
His original A2 release thread is here:
http://forums.bistudio.com/showthread.php?90556-Christian-1987-AddOn-Release-Thread&p=2130298&viewfull=1#post2130298


Robalo: he completely rewrote the config after Ive made a mes of it. Thanks Robalo!

Pomigit: created camo textures for version 2.0!

My and Christian's thanks go to:
'Schmung' for the Mp7 ground base Model !
'The_Tub' for the Mp7 Texture base !
'TheLama' for the Acog/EoTech Model & Texture's !
'Norrin' for the PERFECT Hand-Hold Animation !
'da12thMonkey' for the AWESOME LLM-01 Laser/Light Module Model & Texture!
'LordJarhead' for the AWESOME Sound's & Sound Config Edit!

I personally want to thank: 
1. Christian for giving me permission to port this outstanding addon
2. Alwarren, who has beem guiding me every step of the way
3. Pomigit for creating camo textures
3. Robert Hammer for advice.