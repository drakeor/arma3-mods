Title:		China - People's Liberation Army Infantry
Version: 	1.2
Author: 	Majoris
_________________________________

Description:
- Retextured OPFOR models depicting futuristic Chinese PLA units (approximately 2035); all units and groups can be found under the "Red" faction. Ammo boxes with Chinese weapons can also be found under "Empty".

Storyline
- After nearly 40 years of unprecedented economic and technological advancements, China has deployed expeditionary forces throughout the Mediterranean in support of allied Iran's military campaigns.

Features:
- Arid, Woodland, Universal, and Marine uniforms
- Chinese weapons:
	- QBZ95 Rifle
	- QBZ95B Carbine
	- QJY88 HMG
	- QBB95 LMG
	- QBU88 DMR
- Chinese language/voice replacement
- Asian face replacement


Notes
- Work in progress! Far from complete.
- Not a recreation of modern PLA soldiers; futuristic (2035) and partly fictional
- Weapon models and the Mandarin voice files were imported with permission from the Arma2 VME PLA Mod

Credits:
- VME PLA mod for weapon models, Chinese language pack, and lots of help!
- ShOcKeRoS for his North Korean units and config template
- Saul for his amazing PSD templates
