Hellenic Armed Forces MOD (HAFM) Weapons v1.3 & Units v1.0 pack for ArmA III [Alpha].

Attention : this is an [Alpha] version of this MOD part so don't expect everything to be perfect.


ABOUT

This pack includes :


UNITS

Hellenic Tactical Infantry (Digital Camo)
Hellenic Tactical Infantry (Lizard Camo)
Hellenic Special Operations Squad (Digital Camo)
Hellenic Quick Reaction Force (Lizard Camo)


WEAPONS

M4A1 Rifle.
M4A1-M203 Rifle.
HK MP5-A4 Submachine gun.
M12 SPR Marksman Rifle.
M60E4 Heavy Machinegun.
HK-21 Light Machinegun.
Colt M1911 Pistol.
M72 LAW.

OPTICS

M4 CarryHandle Ironsight Optic.
ACOG Optic.
Red Dot Aimpoint Optic.
Eotech Optic.

MUZZLES

M4 Silencer.
MP5 Silencer.
M1911 Silencer.

FLASHLIGHTS

MP5 handguard fwd flashlight

CRATES

Weapons Box
Ammo Box
Accessories Box
Supply Box


NOTE : No change of classnames, textures, models, or any other part of the mod is permitted without prior written notice by me - email : hacadmin@gmail.com

CLASSNAMES

SIDE : INDEPENDENT

FACTION : HELLAS

UNITS

[Hellenic Tactical Infantry (Digital)]

GR_D_Team_Leader
GR_D_Squad_Leader
GR_D_Machinegunner
GR_D_AutoRifleman
GR_D_Soldier
GR_D_Soldier_AT
GR_D_Medic
GR_D_Soldier_GL
GR_D_Sabot
GR_D_Marksman
GR_D_Engineer

[Hellenic Tactical Infantry (Lizard)]

GR_L_Team_Leader
GR_L_Squad_Leader
GR_L_Machinegunner
GR_L_AutoRifleman
GR_L_Soldier
GR_L_Soldier_AT
GR_L_Medic
GR_L_Soldier_GL
GR_L_Sabot
GR_L_Marksman
GR_L_Engineer

[Hellenic Special Operations Squad (Digital)]

GR_D_ZMAK_Team_Leader
GR_D_ZMAK_Squad_Leader
GR_D_ZMAK_Machinegunner
GR_D_ZMAK_AutoRifleman
GR_D_ZMAK_Soldier
GR_D_ZMAK_Soldier_AT
GR_D_ZMAK_Medic
GR_D_ZMAK_Soldier_GL
GR_D_ZMAK_Sabot
GR_D_ZMAK_Marksman
GR_D_ZMAK_Engineer

[Hellenic Quick Reaction Force (Lizard)]

GR_QRF_TL
GR_QRF_Sabot
GR_QRF_Medic
GR_QRF_AR
GR_QRF_AT
GR_QRF_Soldier
GR_QRF_Marksman


WEAPONS

HAFM_M4A1_EMPTY
HAFM_M4A1_M203
HAFM_M203
HAFM_MP5A4
HAFM_MK12_SPR
HAFM_M60E4
HAFM_HK21
HAFM_Colt1911
HAFM_LAW


MAGAZINES

30Rnd_556x45_STANAG (Default)
1Rnd_HE_Grenade_shell (Default)
UGL_FlareWhite_F (Default)
UGL_FlareGreen_F (Default)
UGL_FlareRed_F (Default)
UGL_FlareYellow_F (Default)
UGL_FlareCIR_F (Default)
1Rnd_Smoke_Grenade_shell (Default)
1Rnd_SmokeRed_Grenade_shell (Default)
1Rnd_SmokeGreen_Grenade_shell (Default)
1Rnd_SmokeYellow_Grenade_shell (Default)
1Rnd_SmokePurple_Grenade_shell (Default)
1Rnd_SmokeBlue_Grenade_shell (Default)
1Rnd_SmokeOrange_Grenade_shell (Default)

HAFM_MP5A4_Mag
HAFM_20rnd_Mk12_762
HAFM_M60_762
HAFM_HK21_762
HAFM_1911_Mag
HAFM_M72_Rocket


OPTICS

HAFM_optic_m4_ironsight_handle
HAFM_optic_m4_aimpoint
HAFM_optic_m4_eotech
HAFM_optic_m4_acog


MUZZLES

HAFM_M4_muzzle_snds_556
HAFM_MP5_muzzle_snds_9mm
HAFM_Colt1911_muzzle_snds_45


FLASHLIGHTS

HAFM_acc_flashlight_mp5


CRATES

HAFM_Weapons_box
HAFM_Ammo_box
HAFM_Acc_box
HAFM_Ord_box
HAFM_Full_box



Aplion for HAFM