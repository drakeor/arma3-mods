
***RPR's Desert Tigerstripe Camo Addon for ARMA3 Alpha V0.5:***


Hi everyone, I finally managed to paint everything in camo pattern!


V0.5 Update:

changelog:
+ added: Camo Helmets
+ added: Camo Vests
+ added: Camo Bergen Backpacks (carried by AT Soldiers)

(note: please delete all old "rpr_TGSD*.pbo" files before intalling the new version, i made some changes to filenames)



***known bugs:***
- Uniform Icon in inventory has white background
- Camo pattern on the vest is a little too big compared to uniform camo pattern


***INSTALLATION***
contains the following files:
rpr_TGSD.pbo: contains the retextures and units **WILL NOT WORK WITHOUT CONFIG PBO**

**IMPORTANT: USE JUST ONE OF THE FOLLOWING CONFIG FILES**

Option 1) (uniform + helmet camo)
rpr_TGSD_cfg.pbo
-Camo Uniform + Helmet
-default weapons
-tan colored Vests
-tan colored Backpacks


Option 2) (uniform & helmet camo + CAF weapons)
rpr_TGSD_cfg_CAF_weaps.pbo
-Camo Uniform + Helmet
-CAF weapons (requires Canadian Armed Forces (http://www.armaholic.com/page.php?id=19420) (also adds Machinegunners as units)
-tan colored Vests
-tan colored Backpacks


Option 3) (full camo)
rpr_TGSD_cfg_camo.pbo
-Camo Uniform + Helmet
-default weapons
-camo Vests
-camo Backpacks


Option 4) (full camo + CAF weapons)
rpr_TGSD_cfg_camo_CAF_weaps.pbo
-Camo Uniform + Helmet
-CAF weapons (requires Canadian Armed Forces (http://www.armaholic.com/page.php?id=19420) (also adds Machinegunners as units)
-camo Vests
-camo Backpacks












***********************************************************************

V0.4 Update!



changelog:
+ added: yet another Camo Pattern based on the "SkyTiger" Pattern by Buckweat
+ added: Assault Packs in all 5 camo patterns
+ added: optional config for Canadian Weapons (needs: the awesome "Canadian Armed Forces Modification ARMA III" by CAF mod team, thanks again!)
+ added: Automatic Rifleman now has rolled up sleeves
+ added: AT Soldiers
+ added: Groups (with mixed headgear)
+ added: Uniform Icon in inventory
* changed: most units now carry HAMR sight and Flashlight in their inventory
* changed: renamed camo pattern "Tigerstripe Digital" to "TigerPat"

known bugs:
- Uniform Icon in inventory has white background


***INSTALLATION***
contains the following files:
rpr_TGSD.pbo: contains the retextures and units **WILL NOT WORK WITHOUT CONFIG PBO**

**IMPORTANT: USE JUST ONE OF THE FOLLOWING CONFIG FILES**

Option 1)
rpr_TGSD_cfg.pbo
-default weapons
-tan colored Assaultpacks



Option 2)
rpr_TGSD_cfg_CAF_weaps.pbo
-CAF weapons (requires Canadian Armed Forces (http://www.armaholic.com/page.php?id=19420)
-tan colored Assaultpacks
(also adds Machinegunners as units)

Option 3)
rpr_TGSD_cfg_camoBackPacks.pbo
-default weapons
-camo Assaultpacks (same camo pattern as unit, 5 available)


Option 4)
rpr_TGSD_cfg_CAF_weaps: config file: 
-CAF weapons (requires Canadian Armed Forces (http://www.armaholic.com/page.php?id=19420)
-camo Assaultpacks (same camo pattern as unit, 5 available)
(also adds Machinegunners as units)







-------------------------------------------------------------
V0.3 update

CONTAINS:----------------
4 different Tigerstripe Camo Uniforms + Units:
+Desert Tigerstripe
+Jungle Tigerstripe (classic)
+Jungle Digital Tigerstripe
+Urban Digital Tigerstripe:

units can be found at "BLUFOR/TigerStripe

UNITS:----------
 Operator
 Automatic Rifleman
 Medic
 Grenadier
 Marksman

each with Cap or Helmet or Beanie Hat (thanks skeeko1337) 

(Nightvision Goggles are in inventory, not on head)


Alpha V0.3:

UPDATE 18.03.2013


changelog:

ADDED:---------------------
+ digital tigerstripe camo
+ digital urban tigerstripe camo



CHANGED:------------------
+ optimized all camo textures, so they look less repetetive an more realistic, with sewn-on pockets

+ changed the Classnames: 
  new listing is now BLUFOR/TigerStripe Units/Infantry Desert
                     BLUFOR/TigerStripe Units/Infantry Jungle
                     BLUFOR/TigerStripe Units/Infantry Jungle (Digital)
                     BLUFOR/TigerStripe Units/Infantry Urban (Digital)

+ changed unit names: Units now named: e.g. (Beanie) Operator / (Helmet) Operator / (Cap) Operator
                 
+ addon is now independent from "Unlock Mod" by skeeko1337




this is a free noncommercial addon!
Big thanks to Bohemia Interactive for making Arma3 and releasing the Alpha
Big thanks to Icewindo for his textures and inspiration for the Config!
Big thanks to Revski for making the Desert Tigerstripe Camo
Big thanks to skeeko1337 for his "Unlock Mod [ALPHA]" and for letting me include the beanie hat in my addon
Big thanks to Revski / sniper_m4 from farcry.crydev.net for the Digital Tigerstripe Camo
Big thanks Buckweat for American Tigerstripe Camo
Big thanks to the CAF mod team for letting me reference their awesome guns from the Canadian Armed Forces pack!


---------------------------------------------------------------------------------
classnames:
Uniforms: 

Desert Tigerstripe: rpr_TGSD_uniform 
Jungle Tigerstripe: rpr_TGSJ_uniform
Digital Jungle Tigerstripe: rpr_TGSDigi_uniform
Digital Urban Tigerstripe: rpr_TGSDigiUrban_uniform

                         
Units:

-----Desert Tigerstripe:-----
(Beanie) Operator:              rpr_TGSD_sold 
(Beanie) Automatic Rifleman:    rpr_TGSD_sold_AR
(Beanie) Medic:                 rpr_TGSD_sold_medic
(Beanie) Marksman:              rpr_TGSD_sold_marksman
(Beanie) Grenadier:             rpr_TGSD_sold_grenadier
(Helmet) Operator:              rpr_TGSD_sold_H 
(Helmet) Automatic Rifleman:    rpr_TGSD_sold_AR_H
(Helmet) Medic:                 rpr_TGSD_sold_medic_H
(Helmet) Marksman:              rpr_TGSD_sold_marksman_H
(Helmet) Grenadier:             rpr_TGSD_sold_grenadier_H
(Cap) Operator:                 rpr_TGSD_sold_C
(Cap) Automatic Rifleman:       rpr_TGSD_sold_AR_C
(Cap) Medic:                    rpr_TGSD_sold_medic_C
(Cap) Marksman:                 rpr_TGSD_sold_marksman_C
(Cap) Grenadier:                rpr_TGSD_sold_grenadier_C


-----Jungle Tigerstripe:-----
(Beanie) Operator:              rpr_TGSJ_sold 
(Beanie) Automatic Rifleman:    rpr_TGSJ_sold_AR
(Beanie) Medic:                 rpr_TGSJ_sold_medic
(Beanie) Marksman:              rpr_TGSJ_sold_marksman
(Beanie) Grenadier:             rpr_TGSJ_sold_grenadier
(Helmet) Operator:              rpr_TGSJ_sold_H 
(Helmet) Automatic Rifleman:    rpr_TGSJ_sold_AR_H
(Helmet) Medic:                 rpr_TGSJ_sold_medic_H
(Helmet) Marksman:              rpr_TGSJ_sold_marksman_H
(Helmet) Grenadier:             rpr_TGSJ_sold_grenadier_H
(Cap) Operator:                 rpr_TGSJ_sold_C
(Cap) Automatic Rifleman:       rpr_TGSJ_sold_AR_C
(Cap) Medic:                    rpr_TGSJ_sold_medic_C
(Cap) Marksman:                 rpr_TGSJ_sold_marksman_C
(Cap) Grenadier:                rpr_TGSJ_sold_grenadier_C

-----Digital Jungle Tigerstripe:------
(Beanie) Operator:              rpr_TGSDigi_sold 
(Beanie) Automatic Rifleman:    rpr_TGSDigi_sold_AR
(Beanie) Medic:                 rpr_TGSDigi_sold_medic
(Beanie) Marksman:              rpr_TGSDigi_sold_marksman
(Beanie) Grenadier:             rpr_TGSDigi_sold_grenadier
(Helmet) Operator:              rpr_TGSDigi_sold_H 
(Helmet) Automatic Rifleman:    rpr_TGSDigi_sold_AR_H
(Helmet) Medic:                 rpr_TGSDigi_sold_medic_H
(Helmet) Marksman:              rpr_TGSDigi_sold_marksman_H
(Helmet) Grenadier:             rpr_TGSDigi_sold_grenadier_H
(Cap) Operator:                 rpr_TGSDigi_sold_C
(Cap) Automatic Rifleman:       rpr_TGSDigi_sold_AR_C
(Cap) Medic:                    rpr_TGSDigi_sold_medic_C
(Cap) Marksman:                 rpr_TGSDigi_sold_marksman_C
(Cap) Grenadier:                rpr_TGSDigi_sold_grenadier_C


-----Digital Urban Tigerstripe:-----
(Beanie) Operator:              rpr_TGSDigiUrban_sold 
(Beanie) Automatic Rifleman:    rpr_TGSDigiUrban_sold_AR
(Beanie) Medic:                 rpr_TGSDigiUrban_sold_medic
(Beanie) Marksman:              rpr_TGSDigiUrban_sold_marksman
(Beanie) Grenadier:             rpr_TGSDigiUrban_sold_grenadier
(Helmet) Operator:              rpr_TGSDigiUrban_sold_H 
(Helmet) Automatic Rifleman:    rpr_TGSDigiUrban_sold_AR_H
(Helmet) Medic:                 rpr_TGSDigiUrban_sold_medic_H
(Helmet) Marksman:              rpr_TGSDigiUrban_sold_marksman_H
(Helmet) Grenadier:             rpr_TGSDigiUrban_sold_grenadier_H
(Cap) Operator:                 rpr_TGSDigiUrban_sold_C
(Cap) Automatic Rifleman:       rpr_TGSDigiUrban_sold_AR_C
(Cap) Medic:                    rpr_TGSDigiUrban_sold_medic_C
(Cap) Marksman:                 rpr_TGSDigiUrban_sold_marksman_C
(Cap) Grenadier:                rpr_TGSDigiUrban_sold_grenadier_C

