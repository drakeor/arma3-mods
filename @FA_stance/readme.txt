ADDON README
=============
Name: FA_stance
Version: 1.1
Authors: Head and harakka
Website: http://www.folkarps.com/forum/viewtopic.php?f=45&t=766


DESCRIPTION
============
This clientside addon adds a UI display showing the player's current stance.

The stance is shown in the bottom left corner and it consists of a human
silhouette and a modifier arrow. If the player is standing in the highest
stance the display shows an upright silhouette with an up arrow, and if the
player is crouched and sidestepping left it'll show a crouched silhouette
with a left arrow, et cetera.

When player is in one of the middle stances, the display becomes partially
transparent. The display is hidden when player is in a vehicle, cutscene view
or similar. The silhouette's texture changes when you're in high or low prone
stance, to remind you that you can't move unless you change your stance.

You can customize the display's position on screen, and whether it fades when
you're in one of the main stances, by editing userconfig\fa\stance_config.hpp.


THANKS
=======
Thanks to Folk ARPS and all users for testing, support and suggestions.
Thanks to JamesF1 for the UI hiding and main loop code.


REQUIREMENTS
=============
Arma 3
Arma 3 version of CBA, http://forums.bistudio.com/showthread.php?147535


INSTALLATION
=============
0. Download and install CBA, linked in Requirements.
1. Place the "@FA_stance" folder into either your game install folder, usually
"C:\Program Files (x86)\Steam\steamapps\common\Arma 3", or the "Arma 3 Alpha"
folder in your "My Documents" folder.
2. Place the "userconfig" folder into your game install folder, usually
"C:\Program Files (x86)\Steam\steamapps\common\Arma 3". 
3. Activate via ingame extensions menu, modline or launcher.
4. Optionally, edit userconfig\fa\stance_config.hpp to change the display's
position on the screen.

TROUBLESHOOTING
================
If your game crashes, you forgot to install the userconfig directory.
See Installation, step 2.


CHANGELOG
==========
1.1: Darkened silhouette outlines are now used instead of a background image to
    improve visibility. Fading can be customized via userconfig.
1.0: Improved modification arrow and background graphics for better visibility,
    display now fades in main stances and hides when player is in vehicle
	or in a cutscene, debug or some other view. Stance display's position can
	be customized by editing userconfig\fa\stance_config.hpp.
0.9: Initial release.


DISCLAIMER
===========
The Software is distributed without any warranty; without even the implied
warranty of merchantability or fitness for a particular purpose. The Software
is not an official addon or tool. Use of the Software (in whole or in part) is
entirely at your own risk.